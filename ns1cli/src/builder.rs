use clap::ArgMatches;

pub struct Builder {
    matches: ArgMatches, // all args send to app
}

impl Builder {
    pub fn new(matches: ArgMatches) -> Builder {
        Builder { matches }
    }
    pub fn run(&self) {
        println!("current application parameters are: {:#?}", self.matches);

        match self.matches.subcommand_name() {
            Some("record") => {
                let record = ns1api::record::Record::new(self.matches.clone());
                record.info();
            }
            Some("zone") => {
                // let zone = zone::Zone::new(self.matches.clone());
            }
            _ => {}
        }
    }
}
