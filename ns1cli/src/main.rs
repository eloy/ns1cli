mod builder;

extern crate ns1api;

use clap::{App, AppSettings, Arg};

fn valid_record(record: String) -> Result<(), String> {
    let types = vec![
        "A", "AAAA", "ALIAS", "AFSDB", "CERT", "CNAME", "DNAME", "HINFO", "MX", "NAPTR", "NS",
        "PTR", "RP", "SPF", "SRV", "TXT",
    ];

    if types.contains(&record.as_str()) {
        Ok(())
    } else {
        Err(String::from(format!(
            "record must have value from {:?}",
            types
        )))
    }
}

#[test]
fn test_valid_record() {
    assert_eq!(Ok(()), valid_record("A".to_string()));
    assert_eq!(Ok(()), valid_record("AAAA".to_string()));
    assert_ne!(Ok(()), valid_record("BB".to_string()));
}

fn main() {
    let matches = App::new("ns1cli")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .version("0.0.1")
        .author("Krzysztof Krzyżaniak (eloy)")
        .args(vec![
            Arg::with_name("config")
                .help("sets the config file to use")
                .takes_value(true)
                .short('c')
                .long("config"),
            Arg::with_name("key")
                .help("sets the API key to use")
                .takes_value(true)
                .short('k')
                .long("key")
                .required(true),
        ])
        .subcommands(vec![
            // subcommand record
            App::new("record")
                .about("manipulate ns1 record")
                .subcommands(vec![
                    App::new("answer").subcommands(vec![
                        App::new("add"),
                        App::new("remove"),
                        App::new("meta").subcommands(vec![App::new("set"), App::new("remove")]),
                    ]),
                    App::new("answers"),
                    App::new("create").about(
                        "record create [options] [--] ZONE DOMAIN TYPE ([--priority=<p>] ANSWER)",
                    ),
                    App::new("delete")
                        .about("record delete [-f] ZONE DOMAIN TYPE")
                        .args(vec![
                            Arg::with_name("force")
                                .help("Force change")
                                .short('f')
                                .long("force"),
                            Arg::with_name("ZONE").required(true).help("name of zone"),
                            Arg::with_name("DOMAIN")
                                .required(true)
                                .help("name of domain"),
                            Arg::with_name("TYPE")
                                .required(true)
                                .help("type of record")
                                .validator(valid_record),
                        ]),
                    App::new("info")
                        .about("record info ZONE DOMAIN TYPE")
                        .args(vec![
                            Arg::with_name("ZONE").required(true).help("name of zone"),
                            Arg::with_name("DOMAIN")
                                .required(true)
                                .help("name of domain"),
                            Arg::with_name("TYPE")
                                .required(true)
                                .help("type of record")
                                .validator(valid_record),
                        ]),
                    App::new("link").about("record link ZONE SOURCE_DOMAIN DOMAIN TYPE"),
                    App::new("meta").subcommands(vec![App::new("set"), App::new("remove")]),
                    App::new("set").about("record set ZONE DOMAIN TYPE options"),
                ]),
            // subcommand zone
            App::new("zone")
                .about("manipulate ns1 zone")
                .subcommands(vec![
                    App::new("create"),
                    App::new("delete"),
                    App::new("info"),
                    App::new("list"),
                    App::new("set"),
                ]),
        ])
        .get_matches();

    builder::Builder::new(matches).run();
}
