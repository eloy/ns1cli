use clap::ArgMatches;
pub struct Zone {
    matches: ArgMatches,
}

impl Zone {
    pub fn new(matches: ArgMatches) -> Zone {
        Zone { matches }
    }

    fn run(&self) {}
}
