use clap::ArgMatches;
use reqwest::{header::HeaderMap, Client};

pub struct Record {
    matches: ArgMatches,
    client: reqwest::Client,
    headers: HeaderMap,
}

impl Record {
    pub fn new(matches: ArgMatches) -> Record {
        let client = Client::new();
        Record {
            matches,
            client,
            headers: HeaderMap::new(),
        }
    }

    pub fn info(&self) {
        let url: &str = &format!(
            "https://api.nsone.net/v1/zones/{}/{}/{}",
            "egnyte.com", "mullenlowe.egnyte.com", "CNAME"
        );
        let record = self.matches.subcommand_matches("record");
        // take api key from parameters
        let key = self.matches.value_of("key");
        println!("record: {:#?}", record);
        println!("key: {:#?}", key);

        // put into headers

        let _res = self.client.get(url);
    }
}
